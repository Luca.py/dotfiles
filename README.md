## **Hello there**
Just run the install script and hopefully every thing will be good :)
Thanks for stopping by 💜


| task         | name                                             |     |
| ------------ | ------------------------------------------------ | --- |
| wm           | [hyprland](https://wiki.hyprland.org)            |     |
| terminal     | [Kitty](https://github.com/alacritty/alacritty)  |     |
| music player | [audacious](https://audacious-media-player.org/) |     |
| bar          | [Waybar](https://github.com/Alexays/Waybar)      |     |


