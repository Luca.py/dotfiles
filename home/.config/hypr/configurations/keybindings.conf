#########################################
# Config :  Hyprland Key bindings		#
# Author :  Luca						#
# Gitlab :  https://gitlab.com/luca.py	#
#########################################

# See https://wiki.hyprland.org/Configuring/Keywords/ for more

# Main Apps
$mainMod = SUPER
$shift = SHIFT
$ctrl = CONTROL
$alt = ALT

# For France Keybord workspaces
$1 = ampersand
$2 = eacute
$3 = quotedbl
$4 = apostrophe
$5 = parenleft
$6 = minus
$7 = egrave
$8 = underscore
$9 = ccedilla
$0 = agrave

# Main Apps
$web_browser = brave
$file_manager = pcmanfm
$terminal = kitty

# Example binds, see https://wiki.hyprland.org/Configuring/Binds/ for more
bind = $mainMod, RETURN, exec, $terminal
#bind = $mainMod, M, exit, 
bind = $ctrl $shift, F, exec, $file_manager
bind = $mainMod, b, exec, $web_browser

bind = $mainMod, P, exec, rofi -show drun # Show the graphical app launcher
bind = $mainMod, R, pseudo, # dwindle
bind = $mainMod, J, togglesplit, # dwindle

bind = , Print, exec, grim -g "$(slurp)" - | swappy -f - # take a screenshot
bind = $mainMod, Insert, exec, cliphist list | rofi -dmenu | cliphist decode | wl-copy

# Move focus with mainMod + arrow keys
bind = $mainMod, left, movefocus, l
bind = $mainMod, right, movefocus, r
bind = $mainMod, up, movefocus, u
bind = $mainMod, down, movefocus, d

# Sets repeatable binds for resizing the active window
binde = $ctrl $alt, right, resizeactive,10 0
binde = $ctrl $alt, left, resizeactive,-10 0
binde = $ctrl $alt, up, resizeactive,0 -10
binde = $ctrl $alt, down, resizeactive,0 10

# ٍScratchpad
bind = $mainMod, M, movetoworkspace,special
bind = $mainMod, S, togglespecialworkspace

# Dispatchers
bind = $mainMod, F, fullscreen,
bind = $mainMod $shift, F, fullscreen, 1
bind = $mainMod, V, togglefloating, 
bind = $mainMod, Y, centerwindow, 
bind = $mainMod $shift, C, killactive, 

# MULTIMEDIA 
bind=, XF86AudioRaiseVolume, exec, pamixer --default-source -i 5
bind=, XF86AudioLowerVolume, exec, pamixer --default-source -d 5
bind=, XF86AudioMute, exec, pamixer --default-source -t
bind=, XF86AudioPlay, exec, playerctl play-pause
bind=, XF86AudioPause, exec, playerctl play-pause
bind=, XF86AudioNext, exec, playerctl next
bind=, XF86AudioPrev, exec, playerctl previous

# Switch workspaces with mainMod + [0-9]
bind = $mainMod, $1, split-workspace, 1
bind = $mainMod, $2, split-workspace, 2
bind = $mainMod, $3, split-workspace, 3
bind = $mainMod, $4, split-workspace, 4
bind = $mainMod, $5, split-workspace, 5
bind = $mainMod, $6, split-workspace, 6
bind = $mainMod, $7, split-workspace, 7
bind = $mainMod, $8, split-workspace, 8
bind = $mainMod, $9, split-workspace, 9
bind = $mainMod, $0, split-workspace, 10

# Move active window to a workspace with mainMod + SHIFT + [0-9]
bind = $mainMod SHIFT, $1, split-movetoworkspacesilent, 1
bind = $mainMod SHIFT, $2, split-movetoworkspacesilent, 2
bind = $mainMod SHIFT, $3, split-movetoworkspacesilent, 3
bind = $mainMod SHIFT, $4, split-movetoworkspacesilent, 4
bind = $mainMod SHIFT, $5, split-movetoworkspacesilent, 5
bind = $mainMod SHIFT, $6, split-movetoworkspacesilent, 6
bind = $mainMod SHIFT, $7, split-movetoworkspacesilent, 7
bind = $mainMod SHIFT, $8, split-movetoworkspacesilent, 8
bind = $mainMod SHIFT, $9, split-movetoworkspacesilent, 9
bind = $mainMod SHIFT, $0, split-movetoworkspacesilent, 10

# Scroll through existing workspaces with mainMod + scroll
bind = $mainMod, mouse_down, split-workspace, e+1
bind = $mainMod, mouse_up, split-workspace, e-1

# Move/resize windows with mainMod + LMB/RMB and dragging
bindm = $mainMod, mouse:272, movewindow
bindm = $mainMod, mouse:273, resizewindow

bind = $mainMod SHIFT, F1, exec, ~/.config/hypr/scripts/gamemode
bind = $alt, tab, exec, swaync-client -t -sw


