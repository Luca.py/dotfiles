require("toggleterm").setup({
	open_mapping = [[<F4>]],
	size = 10,
	shade_terminals = true,
	--direction = "vertical",
	direction = "horizontal",
	-- float_opts = {
	-- 	border = "double",
	-- },
	close_on_exit = true,
})
