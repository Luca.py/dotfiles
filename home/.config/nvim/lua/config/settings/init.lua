local set = vim.opt

set.encoding = "utf-8" 			-- set encoding
set.nu = true 					-- enable line numbers
--set.relativenumber = true 		-- relative line numbers


set.tabstop = 4
set.softtabstop = 4
set.shiftwidth = 4
set.expandtab = true 			-- convert tabs to spaces
set.autoindent = true 			-- auto indentation
set.list = true 				-- show tab characters and trailing whitespace

set.hlsearch = true
set.incsearch = true
set.ignorecase = true
set.smartcase = true

set.termguicolors = true
set.showmode = false
set.splitbelow = true
set.splitright = true
set.wrap = false
set.breakindent = true
set.scrolloff = 5
set.fileencoding = "utf-8"
set.conceallevel = 2

vim.opt.termguicolors = true
